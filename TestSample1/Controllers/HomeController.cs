﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Configuration;
using MyfashionsDB.Models;
using System.Data;
using System.Web.UI.WebControls;

using System.Web.Mvc;
using Newtonsoft.Json;


namespace TestSample1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }
        public ActionResult Products()
        {
            ViewBag.ID = Convert.ToInt64(Request.QueryString["id"]);
            return PartialView();
        }
    }
}

