﻿(function () {
    "use strict";
    angular.module("chandana")
    .controller('menucontroller', function ($scope, $http) {
        $scope.fetch = function () {
            $scope.response = null;
            var base64 = btoa("chandana:123456");
            var base64tonormal = atob(base64);
            $http.get("http://www.vendormp.in/api/MasterCategories/GetAllCategories?relatedcompanyid=1", { headers: { 'Authorization': 'Basic Y2hhbmRhbmE6MTIzNDU2', 'AdminType': 'C' } })
                        .success(function (response) {
                            $scope.message = response.LSTCATEGORIES;
                        })
                        .error(function (response) {
                            $scope.message = response;
                        });
        };
    });

})();